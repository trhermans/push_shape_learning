function renormalized_sc = renormalize_sc(input_sc)
% Renormalize sc to 5 histograms that sums to 0.2 each
    renormalized_sc = zeros(size(input_sc));
    for i=1:5
        renormalized_sc(:,i:5:end) = bsxfun(@rdivide,input_sc(:,i:5:end), sum(input_sc(:,i:5:end),2)+eps)  / i / (1.95 + 1/3);
    end
end