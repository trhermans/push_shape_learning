size_hist = 6
% Do a Gaussian smoothing on the binarized local histogram
reshape_local = double([ 0 0 0 0 0 0 0 0 0 0;
                    0 0 0 0 0 0 0 0 0 0;
                    0 0 0 0 0 0 0 0 0 0;
                    0 0 0 0 0 0 0 0 0 0;
                    0 0 0 1 1 0 0 0 0 0;
                    0 0 0 0 0 1 1 0 0 0;
                    0 0 0 0 0 0 1 1 0 0;
                    0 0 0 0 0 0 0 0 0 0;
                    0 0 0 0 0 0 0 0 0 0;
                    0 0 0 0 0 0 0 0 0 0])

reshape_local = single([ 0 0 0 0 0 0 0 0 0 0;
                    0 0 0 0 0 1 1 0 0 0;
                    0 0 0 0 0 1 1 1 0 0;
                    0 0 0 0 0 1 0 0 0 0;
                    0 0 0 0 1 0 0 0 0 0;
                    0 0 0 0 1 1 0 0 0 0;
                    0 0 0 0 1 0 0 0 0 0;
                    0 0 0 0 1 0 0 0 0 0;
                    0 0 0 1 0 0 0 0 0 0;
                    0 0 0 1 0 0 0 0 0 0])


% sq = squeeze(reshape_local)
ssi_resize = imresize(reshape_local, [size_hist,size_hist])
% ssi = double([ 0 0 0 0 0 0;
%  0 0 0 0 0 0;
%  0 0 0.941358 -0.218108 0 0;
%  0 0 -0.218107 1.05796 0.370371 -0.0205762;
%  0 0 0 0 0 0;
%  0 0 0 0 0 0]);
ssi_smooth = imfilter(ssi_resize,fspecial('gaussian',[5 5],0.2))
ssi = ssi_smooth;
ssi(ssi<0) = 0
l1_normed = bsxfun(@rdivide, ssi, sum(sum(ssi)))
