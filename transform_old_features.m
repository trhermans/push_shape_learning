function new_feats = transform_old_features(old_feats)
local_start = 14;
local_end = 113;
global_start = 127;
local_old_width = 10;
local_new_width = 6;
local_gaussian_size = 5;
local_gaussian_sigma = 0.2;

% Extract the local histogram, discarding the range and variance features
local_old = old_feats(:,local_start:local_end);
local_new = zeros(size(local_old,1), local_new_width*local_new_width);

% Perform Gaussian smoothing on the binarized local histogram
reshape_local = double(reshape(full(local_old), size(local_old,1), local_old_width, local_old_width) > 0);

for j=1:size(local_old, 1)
    sq = squeeze(reshape_local(j,:,:));
    % NOTE: Use to transpose to match C++
    ssi = imresize(sq.', [local_new_width,local_new_width]);
    ssi = imfilter(ssi,fspecial('gaussian',[local_gaussian_size local_gaussian_size], local_gaussian_sigma));
    ssi(ssi<0) = 0;
    local_new(j,:) = ssi(:);
end

% L1 normalize the local feature
local_new = (bsxfun(@rdivide, local_new, sum(local_new,2)));

% Get the data for the global feature histogram, ignoring range and var data
global_new = old_feats(:,global_start:end);

% Binarize global feature
global_new = double(global_new > 0);

% L1 normalize global feature
global_new = (bsxfun(@rdivide, global_new, sum(global_new,2)));

new_feats = [local_new global_new];
end