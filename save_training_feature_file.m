function save_training_feature_file(train_data, file_path)
nrows = size(train_data,1);
[c, r, v] = find(train_data.');
fid = fopen(file_path, 'w');
for i=1:nrows
    c_i = c(r==i);
    v_i = v(r==i);
    for j=1:length(c_i)
        fprintf(fid, '%i:%g ', int64(c_i(j)), v_i(j));
    end
    fprintf(fid, '\n');
end
fclose(fid);
end