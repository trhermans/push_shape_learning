clear

% Switches and params
is_spin = false;
do_cross_val = false;

use_linear_svm = 0;
use_linear_regression = 1;
use_linear_quad = 2;
use_boosting = 3;
use_multi_kernel_svm = 4;
use_rbf_kernel_svm = 5;
classifier_choice = use_multi_kernel_svm;

size_hist = 6;
local_start = 1;
local_end = 100;
global_start = 101;
global_end = 160;
local_feats_only = false;

if is_spin
    gamma_hks = 0.05;
    gamma_sc = 2.5;
    epsilon = 0.2;
    mixture_weight = 0.7;
    c_cost = 1.0;
else
    gamma_hks = 1.0e-14;
    gamma_sc = 2.4;
    mixture_weight = 0.2;
    epsilon = 0.2;
    c_cost = 2.0;
end

if do_cross_val
    kparam = [0.1e-10, 0.5e-10, 0.1e-11, 0.5e-11, 0.1e-12, 0.5e-12, 0.1e-13, 0.5e-13, 0.1e-14, 0.5e-14];
    gammas = [1.0:0.1:3.0];
    epsilons = [0.05:0.05:0.5];
    mixtures = [0.0:0.1:1.0];
    c_costs = [0.5:0.5:5];
    param0 = epsilons;
    param1 = gammas;
    mean_terror = zeros(length(param0),length(param1));
else
    mean_terror = 0.0;
end

% Get the data
read_files_new

% Run the stuff
for ii = 1:size(mean_terror,1)
    if do_cross_val
        epsilon = param0(ii)
    end
    for jj=1:size(mean_terror,2)
        if do_cross_val
            gamma_sc = param1(jj)
        end
        for i=1:num_sets
            leave_one_out_train{i} = cell2mat([inst(1:i-1) inst(i+1:end)]');
            leave_one_out_test{i} = inst{i};
            if is_spin
                leave_one_out_train_lab{i} = transform_spin_target(cell2mat([lab(1:i-1) lab(i+1:end)]'));
                leave_one_out_test_lab{i} = transform_spin_target(lab{i});
                zero_order_regressor{i} = mean(leave_one_out_train_lab{i});
            else
                leave_one_out_train_lab{i} = log(cell2mat([lab(1:i-1) lab(i+1:end)]'));
                leave_one_out_test_lab{i} = log(lab{i});
                zero_order_regressor{i} = mean(leave_one_out_train_lab{i});
            end
            zero_order_error{i} = mean(abs(zero_order_regressor{i} - leave_one_out_test_lab{i}));

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Linear SVM
            switch classifier_choice
             case use_linear_svm
              svmmodel = svmtrain(leave_one_out_train_lab{i}, leave_one_out_train{i}, ...
                                  [sprintf('-s 3 -p %g -t 0 -c %g', epsilon, c_cost)]);
              pred_test{i} = svmpredict(leave_one_out_test_lab{i}, leave_one_out_test{i}, svmmodel);
              terror{i} = mean(abs(pred_test{i} - leave_one_out_test_lab{i}));
              % Save data to disk for later use
              if ~do_cross_val
                  svm_savemodel(svmmodel, sprintf('push_svm_%g.model', i));
                  save_training_feature_file(leave_one_out_train{i}, sprintf('push_svm_%g-feats.txt', i));
              end
             case use_multi_kernel_svm
              % Use Gaussian Kernel on HKS and take weighed sum with X^2 on Shape Context
              K_hks_train = rbf_kernel(leave_one_out_train{i}(:,local_start:local_end), ...
                                       leave_one_out_train{i}(:,local_start:local_end), gamma_hks);
              K_hks_test = rbf_kernel(leave_one_out_test{i}(:,local_start:local_end), ...
                                      leave_one_out_train{i}(:,local_start:local_end), gamma_hks);
              K_sc_train = chi_square_kernel(leave_one_out_train{i}(:,global_start:end),...
                                             leave_one_out_train{i}(:,global_start:end), gamma_sc);
              K_sc_test = chi_square_kernel(leave_one_out_test{i}(:,global_start:end), ...
                                            leave_one_out_train{i}(:,global_start:end), gamma_sc);
              K_train = mixture_weight * K_hks_train + (1 - mixture_weight) *  K_sc_train;
              K_test = mixture_weight * K_hks_test + (1 - mixture_weight) * K_sc_test;

              % Do the learning and prediction
              svmmodel = svmtrain(leave_one_out_train_lab{i}, [(1:size(K_train,1))' double(K_train)], ...
                                  [sprintf('-s 3 -p %g -t 4 -c %g', epsilon, c_cost)]);
              pred_test{i} = svmpredict(leave_one_out_test_lab{i}, [(1:size(K_test,2))' double(K_test)'], svmmodel);
              terror{i} = mean(abs(pred_test{i} - leave_one_out_test_lab{i}));

              % Save data to disk for later use
              if ~do_cross_val
                  svm_savemodel(svmmodel, sprintf('push_svm_%g.model', i));
                  save_training_feature_file(leave_one_out_train{i}, sprintf('push_svm_%g-feats.txt', i));
              end
             case use_rbf_kernel_svm
              svmmodel = svmtrain(leave_one_out_train_lab{i}, leave_one_out_train{i}, [sprintf('-s 3 -p %g -t 2 -c %g -g %g', epsilon, c_cost, gamma_hks)]);
              pred_test{i} = svmpredict(leave_one_out_test_lab{i}, leave_one_out_test{i}, svmmodel);
              terror{i} = mean(abs(pred_test{i} - leave_one_out_test_lab{i}));
             case use_linear_regression
              % Linear Regression
              LinReg_obj = LinearRegressor_Data((full(leave_one_out_train{i})), leave_one_out_train_lab{i});
              w = LinReg_obj.Regress(0.5);
              pred_test{i} = w(1) + (leave_one_out_test{i}) * w(2:end);
              terror{i} = mean(abs(pred_test{i} - leave_one_out_test_lab{i}));
             case use_linear_quad
              % Linear Regression with quadratic mapping
              LinReg_obj = LinearRegressor_Data(expand_linear_to_quad((full(leave_one_out_train{i}))), ...
                                                leave_one_out_train_lab{i});
              w = LinReg_obj.Regress(20);
              pred_test{i} = w(1) + expand_linear_to_quad((leave_one_out_test{i})) * w(2:end);
              terror{i} = mean(abs(pred_test{i} - leave_one_out_test_lab{i}));
             case use_boosting
              % Boosting
              [trees,pred_test{i},terror{i}] = l2boost(full(leave_one_out_train{i}),...
                                                       leave_one_out_train_lab{i}, ...
                                                       full(leave_one_out_test{i}),...
                                                       leave_one_out_test_lab{i},7,1,5e-3);
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Sqrting
            %    leave_one_out_train{i}(:,[local_start:local_end global_start:end]) = sqrt(leave_one_out_train{i}(:,[local_start:local_end global_start:end]));
            %    leave_one_out_test{i}(:,[local_start:local_end global_start:end]) = sqrt(leave_one_out_test{i}(:,[local_start:local_end global_start:end]));
            %    leave_one_out_train{i} = leave_one_out_train{i}(:,[local_start:local_start+size_hist*size_hist-1 global_start:end]);
            %    leave_one_out_test{i} = leave_one_out_test{i}(:,[local_start:local_start+size_hist*size_hist-1 global_start:end]);
        end
        terror = cell2mat(terror);
        if do_cross_val
            mean_terror(ii, jj) = mean(terror,2)
            mean(terror,2);
            clear terror;
        else
            zero_order_error = cell2mat(zero_order_error);
            mean_terror = mean(terror,2);
            mean_zero_terror = mean(zero_order_error,2);
        end
    end % jj
end % ii

if ~do_cross_val
    report_planning_error;
    terror
    mean_terror
    zero_order_error
    mean_zero_terror
else
    [min_value, idx] = min(mean_terror(:));
    min_value
    [i,j] = ind2sub(size(mean_terror), idx)
    best_param0 = param0(i)
    best_param1 = param1(j)
end