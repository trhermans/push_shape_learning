
function [plan_error] = get_planning_error(pred_labels, gt_labels, is_spin)
k=10;
k = min(k, length(gt_labels));
y_hat = inverse_transform_spin_target(pred_labels);
y = inverse_transform_spin_target(gt_labels);
if is_spin
    [a, idx] = sort(y_hat, 'descend');
else
    [a, idx] = sort(y_hat);
end
topk = idx(1:k);
if is_spin
    gt = sort(y, 'descend');
else
    gt = sort(y);
end
plan_errors=y(topk).'
top_gt = gt(1:k).'
plan_error = plan_errors(1);
end