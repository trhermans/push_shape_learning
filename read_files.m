num_sets = 6
[lab{1}, inst{1}] = libsvmread('camcorder_local_and_global100.txt');
[lab{2}, inst{2}] = libsvmread('food_box_local_and_global100.txt');
[lab{3}, inst{3}] = libsvmread('large_brush_local_and_global100.txt');
%[lab{4}, inst{4}] = libsvmread('shampoo_local_and_global100.txt');
[lab{4}, inst{4}] = libsvmread('small_brush_local_and_global100.txt');
[lab{5}, inst{5}] = libsvmread('soap_box_local_and_global100.txt');
%[lab{7}, inst{7}] = libsvmread('teddy_bear_local_and_global100.txt');
[lab{6}, inst{6}] = libsvmread('toothpaste_local_and_global100.txt');
for i=1:num_sets
    inst{i} = [inst{i} zeros(size(inst{i},1),184-size(inst{i},2))];
end

% Clear out the old junk we don't need and do the resizing
for i=1:num_sets
    inst{i} = transform_old_features(inst{i});
end

% inst{1} = load('camcorder_new_raw.txt');
% inst{2} = load('food_box_new_raw.txt');
% inst{3} = load('large_brush_new_raw.txt');
% inst{4} = load('shampoo_new_raw.txt');
% inst{5} = load('small_brush_new_raw.txt');
% inst{6} = load('soap_box_new_raw.txt');
% inst{7} = load('teddy_bear_new_raw.txt');
% inst{8} = load('toothpaste_new_raw.txt');
