clear

is_spin = true;

read_files_new

local_start = 1;
local_end = 36;
global_start = 37;

% NOTE: Set training sets to use here
training_indices_sets = {[class_indices.camcorder], [class_indices.food_box], [class_indices.large_brush], [class_indices.small_brush], [class_indices.soap_box], [class_indices.toothpaste]};

training_set_sizes = [5:5:85];
% training_set_sizes = [0];
% training_set_sizes = [5];

if is_spin
    % Transformed targets
    gamma_local=0.05; % Cross-val
    gamma_global=2.5; % Cross-val
    epsilon = 0.2; % Cross-val
    mixture_weight = 0.7;
    c_cost = 1.0;
else
    gamma_local=2.5;
    gamma_global=2.0;
    mixture_weight = 0.7;
    epsilon = 0.3;
    c_cost = 2.0;
end

% Run the stuff
for i=1:length(training_indices_sets)
    for training_set_size=training_set_sizes
    training_indices = training_indices_sets{i};
    testing_indices = get_testing_indices_from_training_indices(training_indices, num_sets);

    if training_set_size > 0
        train_set_raw = cat(1, inst{training_indices});
        full_set_size = size(train_set_raw, 1);
        train_set_lab_raw = cat(1, lab{training_indices});
        % Make sure we don't try and get more data than we have
        if training_set_size > full_set_size
            continue;
        end
        training_set_size = min(training_set_size, full_set_size);
        raw_indices = datasample([1:full_set_size], training_set_size, 'Replace', false);
        train_set{i} = train_set_raw(raw_indices,:);
        train_set_lab{i} = train_set_lab_raw(raw_indices);
        % Augment test set with the remaining training set indices
        leftover_indices = [];
        for k=[1:full_set_size]
            if sum(raw_indices == k) < 1
                leftover_indices = [leftover_indices k];
            end
        end
        test_set_aug = train_set_raw(leftover_indices, :);
        test_set_lab_aug = train_set_lab_raw(leftover_indices);
    else % Use all the training data
        train_set{i} = cat(1, inst{training_indices});
        train_set_lab{i} = cat(1, lab{training_indices});
        test_set_aug = [];
        test_set_lab_aug = [];
    end
    display(sprintf('\nTraining on %s with %d inputs\n', sprintf(' %s', class_labels{training_indices}), size(train_set{i}, 1)));

    if is_spin
        train_set_lab{i} = transform_spin_target(train_set_lab{i});
    else
        train_set_lab{i} = log(train_set_lab{i});
    end
    zero_order_regressor{i} = mean(train_set_lab{i});
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Kernel SVM learning
    K_train = chi_square_kernel(train_set{i}(:,global_start:end),train_set{i} ...
                                (:,global_start:end), gamma_global);
    K_train2 = chi_square_kernel(train_set{i}(:,local_start:local_end),train_set{i} ...
                                 (:,local_start:local_end), gamma_local);
    K_train = mixture_weight * K_train + (1 - mixture_weight) *  K_train2;
    svmmodel = svmtrain(train_set_lab{i}, [(1:size(K_train,1))' double(K_train)], [sprintf('-s 3 -p %g -t 4 -c %g', epsilon, c_cost)]);
    % Save data to disk for later use
    if ~exist('variable_class_files', 'dir')
        mkdir('variable_class_files');
    end
    if training_set_size > 0
        model_file_name = sprintf('variable_class_files/push_svm_%i%s.model', training_set_size, sprintf('_%s', class_labels{training_indices}));
        feat_file_name = sprintf('variable_class_files/push_svm_%i%s-feats.txt', training_set_size, sprintf('_%s', class_labels{training_indices}));
    else
        model_file_name = sprintf('variable_class_files/push_svm%s.model', sprintf('_%s', class_labels{training_indices}));
        feat_file_name = sprintf('variable_class_files/push_svm%s-feats.txt', sprintf('_%s', class_labels{training_indices}));
    end
    svm_savemodel(svmmodel, model_file_name);
    save_training_feature_file(train_set{i}, feat_file_name);

    num_testing_sets = length(testing_indices);
    if length(test_set_aug) > 0
        num_testing_sets = num_testing_sets+1;
    end
    % Loop heere over each of the test sets
    for j=1:num_testing_sets
        if j > length(testing_indices)
            display(sprintf('\nTesting on augmented set\n'));
            test_set = test_set_aug;
            test_set_lab = test_set_lab_aug;
        else
            test_idx = testing_indices(j);
            display(sprintf('\nTesting on %s\n', class_labels{test_idx}));
            test_set = inst{test_idx};
            test_set_lab = lab{test_idx};
        end
        if is_spin
            test_set_lab  = transform_spin_target(test_set_lab);
        else
            test_set_lab  = log(test_set_lab);
        end
        K_test = chi_square_kernel(test_set(:,global_start:end),train_set{i}(:,global_start:end), gamma_global);
        K_test2 = chi_square_kernel(test_set(:,local_start:local_end),train_set{i}(:,local_start:local_end), gamma_local);
        K_test = mixture_weight * K_test + (1 - mixture_weight) * K_test2;

        pred_test{i}{j} = svmpredict(test_set_lab, [(1:size(K_test,2))' double(K_test)'], svmmodel);
        terror{i}{j} = mean(abs(pred_test{i}{j} - test_set_lab));
        zero_order_error{i}{j} = mean(abs(zero_order_regressor{i} - test_set_lab));
        [planning_error{i}{j}] = get_planning_error(pred_test{i}{j}, test_set_lab, is_spin);
        zero_order_planning_error{i}{j} = mean(exp(test_set_lab));
    end

    % Format results for display and saving
    format shortG;
    if length(test_set_aug) > 0
        header_str = strcat(sprintf('\t%s', class_labels{testing_indices}), strcat(sprintf('\taugmented'),sprintf('\tmean')));
    else
        header_str = strcat(sprintf('\t%s', class_labels{testing_indices}), sprintf('\tmean'));
    end
    learned_str = strcat(sprintf('learned: '), sprintf('%f\t', cell2mat(terror{i})), sprintf('\t%f',mean([terror{i}{:}])));
    zero_order_str = strcat(sprintf('0 order: '), sprintf('%f\t', cell2mat(zero_order_error{i})), sprintf('\t%f',mean([zero_order_error{i}{:}])));
    planning_error_str = strcat(sprintf('planning: '), sprintf('%f\t', cell2mat(planning_error{i})), sprintf('\t%f',mean([planning_error{i}{:}])));
    zero_planning_error_str = strcat(sprintf('0 planning: '), sprintf('%f\t', cell2mat(zero_order_planning_error{i})), sprintf('\t%f',mean([zero_order_planning_error{i}{:}])));

    % Save these results to disk and display to terminal
    if training_set_size > 0
        results_file_name = sprintf('variable_class_files/push_svm_%i%s_results.txt', training_set_size, sprintf('_%s', class_labels{training_indices}));
    else
        results_file_name = sprintf('variable_class_files/push_svm%s_results.txt', sprintf('_%s', class_labels{training_indices}));
    end
    display(header_str);
    display(learned_str);
    display(zero_order_str);
    display(planning_error_str);
    display(zero_planning_error_str);
    results_file_id = fopen(results_file_name,'w');
    fprintf(results_file_id, strcat(header_str,'\n'));
    fprintf(results_file_id, strcat(learned_str,'\n'));
    fprintf(results_file_id, strcat(zero_order_str,'\n'));
    fprintf(results_file_id, strcat(planning_error_str,'\n'));
    fprintf(results_file_id, strcat(zero_planning_error_str,'\n'));
    fclose(results_file_id);
end
end