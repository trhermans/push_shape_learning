num_sets=6;
% Rotate data
if is_spin
    [lab{1}, inst{1}] = libsvmread('camcorder0.txt');
    [lab{2}, inst{2}] = libsvmread('food_box0.txt');
    [lab{3}, inst{3}] = libsvmread('large_brush0.txt');
    [lab{4}, inst{4}] = libsvmread('small_brush0.txt');
    [lab{5}, inst{5}] = libsvmread('soap_box0.txt');
    [lab{6}, inst{6}] = libsvmread('toothpaste0.txt');
else
    % Straight line data
    [lab{1}, inst{1}] = libsvmread('camcorder_new_feats_cpp.txt');
    [lab{2}, inst{2}] = libsvmread('food_box_new_feats_cpp.txt');
    [lab{3}, inst{3}] = libsvmread('large_brush_new_feats_cpp.txt');
    [lab{4}, inst{4}] = libsvmread('small_brush_new_feats_cpp.txt');
    [lab{5}, inst{5}] = libsvmread('soap_box_new_feats_cpp.txt');
    [lab{6}, inst{6}] = libsvmread('toothpaste_new_feats_cpp.txt');
end
for i=1:num_sets
    inst{i} = [inst{i} zeros(size(inst{i},1), global_end-size(inst{i},2))];
    % HACK: Uncomment for HKS / Local feats only
    if local_feats_only
        inst{i} = inst{i}(:,1:local_end);
    end
end

class_indices = struct('camcorder',1,'food_box',2,'large_brush',3,'small_brush',4,'soap_box',5,'toothpaste',6);
class_labels = {'camcorder','food_box','large_brush','small_brush','soap_box','toothpaste'};