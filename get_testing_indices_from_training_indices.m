function testing_indices = get_testing_indices_from_training_indices(training_indices, num_sets)
testing_indices = [];

for i=1:num_sets
    if sum(training_indices==i) == 0
        testing_indices = [testing_indices i];
    end
end

end