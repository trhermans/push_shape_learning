clear

is_spin = false;
size_hist = 6;
local_start = 1;
local_end = 100;
global_start = 101;
global_end = 160;

read_files_new



if is_spin
    % Transformed targets
    gamma_local=0.05; % Cross-val
    gamma_global=2.5; % Cross-val
    epsilon = 0.2; % Cross-val
    mixture_weight = 0.7;
    c_cost = 1.0;
else
    gamma_local=2.5;
    gamma_global=2.0;
    mixture_weight = 0.7;
    epsilon = 0.3;
    c_cost = 2.0;
end

do_cross_val=false;
if do_cross_val
    % kparam = [0.05 0.1 0.25 0.5 0.75 1 1.25 1.5 2 2.5];
    kparam = [0.05 0.1 0.125 0.15 0.175 0.2 0.25 0.3 0.4 0.5 0.75 1 1.25 1.5 2 2.5];
    % kparam = [0.05:0.05:0.5];
    % kparam = 2.0;
    epsilons = [0.05:0.05:0.5];
    mixtures = [0.1:0.1:1.0];
    c_costs = [0.5:0.5:5];
    mean_terror = zeros(length(mixtures),length(c_costs));
else
    mixtures = mixture_weight;
    kparam = gamma_local;
    epsilons = epsilon;
    c_costs = c_cost;
    mean_terror = 0.0;
end

% Run the stuff
for ii = 1:size(mean_terror,1)
    if do_cross_val
        % gamma_global=kparam(ii)
        mixture_weight = mixtures(ii)
    end
    for jj=1:size(mean_terror,2)
        if do_cross_val
            c_cost = c_costs(jj)
            % gamma_global=kparam(jj)
            % gamma_local=kparam(jj)
            % epsilon=epsilons(jj)
        end
        for kk=1:size(mean_terror,3)
            % gamma_global=kparam(kk)
        for i=1:num_sets
            leave_one_out_train{i} = cell2mat([inst(1:i-1) inst(i+1:end)]');
            leave_one_out_test{i} = inst{i};
            if is_spin
                leave_one_out_train_lab{i} = transform_spin_target(cell2mat([lab(1:i-1) lab(i+1:end)]'));
                leave_one_out_test_lab{i} = transform_spin_target(lab{i});
                % zero_order_regressor{i} = mean(inverse_transform_spin_target(leave_one_out_train_lab{i}));
                zero_order_regressor{i} = mean(leave_one_out_train_lab{i});
            else
                leave_one_out_train_lab{i} = log(cell2mat([lab(1:i-1) lab(i+1:end)]'));
                leave_one_out_test_lab{i} = log(lab{i});
                zero_order_regressor{i} = mean(leave_one_out_train_lab{i});
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Kernel SVM
            K_train = chi_square_kernel(leave_one_out_train{i}(:,global_start:end),leave_one_out_train{i} ...
                                        (:,global_start:end), gamma_global);
            K_test = chi_square_kernel(leave_one_out_test{i}(:,global_start:end),leave_one_out_train{i}(:,global_start:end), ...
                                       gamma_global);
            K_train2 = chi_square_kernel(leave_one_out_train{i}(:,local_start:local_end),leave_one_out_train{i} ...
                                         (:,local_start:local_end), gamma_local);
            K_test2 = chi_square_kernel(leave_one_out_test{i}(:,local_start:local_end),leave_one_out_train{i} ...
                                        (:,local_start:local_end), gamma_local);
            % C = 0.5 for * 100
            %    svmmodel = libsvm_train(leave_one_out_train_lab{i}, [(1:size(K_train,1))' double(K_train)],'-s 3 -t 4 -c 0.5');
            % C = 5 for log
            K_train = mixture_weight * K_train + (1 - mixture_weight) *  K_train2;
            K_test = mixture_weight * K_test + (1 - mixture_weight) * K_test2;
            % svmmodel = libsvm_train(leave_one_out_train_lab{i}, [(1:size(K_train,1))' double(K_train)],['-s 3 -p 0.3 -t 4 -c 2']);
            svmmodel = svmtrain(leave_one_out_train_lab{i}, [(1:size(K_train,1))' double(K_train)], [sprintf('-s 3 -p %g -t 4 -c %g', epsilon, c_cost)]);
            % pred_test{i} = libsvm_predict(leave_one_out_test_lab{i}, [(1:size(K_test,2))' double(K_test)'], svmmodel);
            pred_test{i} = svmpredict(leave_one_out_test_lab{i}, [(1:size(K_test,2))' double(K_test)'], svmmodel);
            % pred_test{i} = pred_test{i} + test_out;
            % terror{i} = mean(abs(inverse_transform_spin_target(pred_test{i}) - inverse_transform_spin_target(leave_one_out_test_lab{i})));
            terror{i} = mean(abs(pred_test{i} - leave_one_out_test_lab{i}));
            zero_order_error{i} = mean(abs(zero_order_regressor{i} - leave_one_out_test_lab{i}));

            % Save data to disk for later use
            if ~do_cross_val
                svm_savemodel(svmmodel, sprintf('push_svm_%g.model', i));
                save_training_feature_file(leave_one_out_train{i}, sprintf('push_svm_%g-feats.txt', i));
            end

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Sqrting
            %    leave_one_out_train{i}(:,[local_start:local_end global_start:end]) = sqrt(leave_one_out_train{i}(:,[local_start:local_end global_start:end]));
            %    leave_one_out_test{i}(:,[local_start:local_end global_start:end]) = sqrt(leave_one_out_test{i}(:,[local_start:local_end global_start:end]));
            %    leave_one_out_train{i} = leave_one_out_train{i}(:,[local_start:local_start+size_hist*size_hist-1 global_start:end]);
            %    leave_one_out_test{i} = leave_one_out_test{i}(:,[local_start:local_start+size_hist*size_hist-1 global_start:end]);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Boosting
            %    [trees,pred_test{i},terror{i}] = l2boost(full(leave_one_out_train{i}),...
            %    leave_one_out_train_lab{i}, full(leave_one_out_test{i}),...
            %    leave_one_out_test_lab{i},7,1,5e-3);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Linear Regression
            %      LinReg_obj = LinearRegressor_Data((full(leave_one_out_train{i})), leave_one_out_train_lab{i});
            %      w = LinReg_obj.Regress(0.5);
            %      test_out = w(1) + (leave_one_out_test{i}) * w(2:end);
            %      terror{i} = mean(abs(test_out - leave_one_out_test_lab{i}));
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Linear Regression with quadratic mapping
            %      LinReg_obj = LinearRegressor_Data(expand_linear_to_quad((full(leave_one_out_train{i}))), leave_one_out_train_lab{i});
            %      w = LinReg_obj.Regress(20);
            %      test_out = w(1) + expand_linear_to_quad((leave_one_out_test{i})) * w(2:end);
            %      terror{i} = mean(abs(test_out - leave_one_out_test_lab{i}));

        end
        terror = cell2mat(terror);
        zero_order_error = cell2mat(zero_order_error);
        if do_cross_val
            mean_terror(ii, jj, kk) = mean(terror,2)
            mean(terror,2)
            clear terror;
        else
            mean_terror = mean(terror,2);
            mean_zero_terror = mean(zero_order_error,2);
        end
        end % kk
    end % jj
end % ii

if ~do_cross_val
    % report_planning_error;
    terror
    mean_terror
    zero_order_error
    mean_zero_terror
else
    [min_value, idx] = min(mean_terror(:));
    min_value
    [i,j] = ind2sub(size(mean_terror), idx)
    % best_global = kparam(i)
    % best_local = kparam(j)
    best_cost = c_costs(j)
    best_mixture = mixtures(i)
end