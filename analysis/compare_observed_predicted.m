classes = {'Camcorder', 'Food Box', 'Large Brush', 'Small Brush', 'Soap Box', 'Toothpaste'};

learned_classes = {'camcorder1', 'food_box1', 'large_brush1', 'small_brush1', 'soap_box1', 'toothpaste1'};
learned_base = '/home/thermans/Dropbox/Data/ichr2013-results/hold_out_straight_line_results/analysis/';
limits = [0, 0.05];
xlimits = [0, 0.025];
for i = 1:length(learned_classes)
    learned_name = [learned_base, learned_classes{i}, '.txt'];
    learned_file = fopen(learned_name);
    learned_data = textscan(learned_file, '%f %f');
    fclose(learned_file);

    predicted = learned_data{1};
    observed = learned_data{2};
    perror = abs(predicted-observed)
    mean_error = mean(perror)

    figure;
    hold on;
    plot(predicted, observed, 'x');
    plot([0, 1], [0, 1], 'r--');
    xlabel('Predicted score');
    ylabel('Observed score');
    title(['Predicted vs Observed Pushing Scores for ', classes{i}]);
    xlim(xlimits);
    ylim(limits);
end