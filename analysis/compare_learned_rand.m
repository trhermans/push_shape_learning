use_final_error = true;
use_spin = true;

classes = {'Camcorder', 'Food Box', 'Large Brush', 'Small Brush', 'Soap Box', 'Toothpaste'};

if use_spin
    learned_base = '/home/thermans/Dropbox/Data/ichr2013-results/rotate_to_heading_test_results/analysis/';
    rand_base = '/home/thermans/Dropbox/Data/ichr2013-results/rotate_to_heading_rand_results/analysis/';
    rand_classes = {'camcorder0', 'food_box0', 'large_brush0', 'small_brush0', 'soap_box0', 'toothpaste0'};
    learned_classes = {'camcorder0', 'food_box0', 'large_brush0', 'small_brush0', 'soap_box0', 'toothpaste0'};
else
    learned_base = '/home/thermans/Dropbox/Data/ichr2013-results/hold_out_straight_line_results/analysis/';
    rand_base = '/home/thermans/Dropbox/Data/ichr2013-results/rand_straight_line_results/analysis/';
    rand_classes = {'camcorder0', 'food_box0', 'large_brush0', 'small_brush0', 'soap_box0', 'toothpaste0'};
    learned_classes = {'camcorder1', 'food_box1', 'large_brush1', 'small_brush1', 'soap_box1', 'toothpaste1'};
end

idx_limits = [1, 15];
if use_spin
    performance_limits = [0, pi];
elseif use_final_error
    performance_limits = [0, 30.];
else
    performance_limits = [0, 6.0];
end

mean_errors = zeros(2,1+length(learned_classes));
median_errors = zeros(2,1+length(learned_classes));
min_errors = zeros(2,1+length(learned_classes));
max_errors = zeros(2,1+length(learned_classes));

for i = 1:length(learned_classes)
    % Build file names
    display(learned_classes{i});

    if use_final_error
        learned_name = [learned_base, learned_classes{i}, '-final-error.txt'];
        rand_name = [rand_base, rand_classes{i}, '-final-error.txt'];
    else
        learned_name = [learned_base, learned_classes{i}, '.txt'];
        rand_name = [rand_base, rand_classes{i}, '.txt'];
    end

    % Read data
    learned_file = fopen(learned_name);
    rand_file = fopen(rand_name);
    if use_final_error
        learned_data = textscan(learned_file, '%f');
        rand_data = textscan(rand_file, '%f');
    else
        learned_data = textscan(learned_file, '%f %f');
        rand_data = textscan(rand_file, '%f %f');
    end
    fclose(learned_file);
    fclose(rand_file);

    % Draw histograms of same dimensions
    if use_final_error
        data_idx = 1;
    else
        data_idx = 2;
    end
    if use_spin & ~use_final_error
        rand_scores = sort(rand_data{data_idx}, 'descend');
        learned_scores = sort(learned_data{data_idx}, 'descend');
    else
        rand_scores = sort(rand_data{data_idx})
        learned_scores = sort(learned_data{data_idx})
        if ~use_spin
            learned_scores = learned_scores*100.;
            rand_scores = rand_scores*100.;
        end
    end
    % rand_scores = rand_data{data_idx};
    % learned_scores = learned_data{data_idx};
    all_learned_scores{i} = learned_scores;
    all_rand_scores{i} = rand_scores;

    median_errors(1,i+1) = median(learned_scores);
    median_errors(2,i+1) = median(rand_scores);
    mean_errors(1,i+1) = mean(learned_scores);
    mean_errors(2,i+1) = mean(rand_scores);
    min_errors(1,i+1) = min(learned_scores);
    min_errors(2,i+1) = min(rand_scores);
    max_errors(1,i+1) = max(learned_scores);
    max_errors(2,i+1) = max(rand_scores);

    cur_fig = figure;
    hold on;
    % title([ classes{i}]);
    plot( [1:length(learned_scores)],learned_scores, 'r');
    plot([1:length(rand_scores)], rand_scores, '--b');
    plot([1:length(learned_scores)], learned_scores, 'rx');
    plot([1:length(rand_scores)], rand_scores, 'bx');
    if use_spin & ~use_final_error
        legend('Learned','Rand','Location','NorthEast');
    else
        legend('Learned','Rand','Location','SouthEast');
    end

    % X = [1:length(learned_scores)]
    % Y = [learned_scores.'; rand_scores.'].'
    % bar(X, Y,1.,'grouped');
    % if use_spin & ~use_final_error
    %     legend('Learned','Rand','Location','NorthEast');
    % else
    %     legend('Learned','Rand','Location','NorthWest');
    % end

    xlabel('Num Trials');
    if use_final_error
        if use_spin
            ylabel('Final Error (Rad)');
        else
            ylabel('Final Error (cm)');
        end
    else
        ylabel('Performance');
    end
    ylim(performance_limits);
    xlim(idx_limits);

    if use_final_error
        fig_name = [learned_base, learned_classes{i}, '-final-error.pdf']
    else
        fig_name = [learned_base, learned_classes{i}, '.png']
    end

    saveas(cur_fig, fig_name);
end
mean_errors(1,1) = mean(mean_errors(1,2:end));
mean_errors(2,1) = mean(mean_errors(2,2:end));
median_errors(1,1) = mean(median_errors(1,2:end));
median_errors(2,1) = mean(median_errors(2,2:end));
min_errors(1,1) = mean(min_errors(1,2:end));
min_errors(2,1) = mean(min_errors(2,2:end));
max_errors(1,1) = mean(max_errors(1,2:end));
max_errors(2,1) = mean(max_errors(2,2:end));
%mean_errors*180./pi
mean_errors
median_errors
min_errors
max_errors
