k=3;
plan_error = zeros(k,num_sets+1);
top_gt = zeros(k,num_sets+1);
zero_order_planning = zeros(1,num_sets+1);
for i=1:num_sets
    % class_labels{i}
    y_hat = inverse_transform_spin_target(pred_test{i});
    y = inverse_transform_spin_target(leave_one_out_test_lab{i});
    if is_spin
        [a, idx] = sort(y_hat, 'descend');
    else
        [a, idx] = sort(y_hat);
    end
    topk = idx(1:k);
    if is_spin
        gt = sort(lab{i}, 'descend');
    else
        gt = sort(lab{i});
    end
    plan_error(:,i)=lab{i}(topk)*100.;
    top_gt(:,i) = gt(1:k)*100.;
    zero_order_planning(i) = mean(y)*100.;
end
plan_error(:,num_sets+1) = mean(plan_error(:,1:num_sets),2);
top_gt(:,num_sets+1) = mean(top_gt(:,1:num_sets),2);
zero_order_planning(:,num_sets+1) = mean(zero_order_planning(:,1:num_sets),2);
% top_gt
plan_error
zero_order_planning