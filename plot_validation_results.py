#!/usr/bin/env python
import sys
import subprocess
import os
import matplotlib.pyplot as plotter

_CLASS_LABELS = ['camcorder', 'food_box', 'large_brush', 'small_brush', 'soap_box', 'toothpaste']

def read_svm_results_file(file_name):
    res_file = open(file_name, 'r')
    header = res_file.readline().split()

    learned_error_raw = res_file.readline().split()
    learned_error = [learned_error_raw[0].split(':')[1]]
    learned_error.extend(learned_error_raw[1:])
    learned_error = [float(l) for l in learned_error]

    z_order_error_raw = res_file.readline().split()
    z_order_error = [z_order_error_raw[1].split(':')[1]]
    z_order_error.extend(z_order_error_raw[2:])
    z_order_error = [float(o) for o in z_order_error]

    planning_error_raw = res_file.readline().split()
    planning_error = [planning_error_raw[0].split(':')[1]]
    planning_error.extend(planning_error_raw[1:])
    planning_error = [float(l) for l in planning_error]

    z_planning_error_raw = res_file.readline().split()
    z_planning_error = [z_planning_error_raw[1].split(':')[1]]
    z_planning_error.extend(z_planning_error_raw[2:])
    z_planning_error = [float(o) for o in z_planning_error]

    return (learned_error, z_order_error, planning_error, z_planning_error, header)

def parse_training_set_size(file_name):
    set_size = file_name.split('_')[2]
    try:
        set_size = int(set_size)
    except ValueError:
        return None
    return set_size

def sort_files_by_training_set_size(c_files):
    c_files_sorted = []
    c_files_set_sizes = []
    for f in c_files:
        cur_set_size = parse_training_set_size(f)
        if cur_set_size is None:
            continue
        if len(c_files_sorted) < 1:
            c_files_sorted.append(f)
            c_files_set_sizes.append(cur_set_size)
        else:
            # Do insertion sort
            file_added = False
            for j, c_size in enumerate(c_files_set_sizes):
                if cur_set_size < c_size:
                    c_files_set_sizes.insert(j, cur_set_size)
                    c_files_sorted.insert(j, f)
                    file_added = True
                    break
            if not file_added:
                c_files_set_sizes.append(cur_set_size)
                c_files_sorted.append(f)

    return (c_files_sorted, c_files_set_sizes)

def plot_class_results(results, set_sizes, train_class):
    # TODO: Send in file name to get class labels
    # Reorganize results as a function of training set size
    learned_results = []
    zero_order_results = []
    test_labels = results[0][4]
    for r in results:
        learned = r[2]
        zero = r[3]
        if len(learned_results) < 1:
            for l in learned:
                learned_results.append([l])
            for z in zero:
                zero_order_results.append([z])
        else:
            for i, l in enumerate(learned):
                learned_results[i].append(l)
            for i, z in enumerate(zero):
                zero_order_results[i].append(z)
    print '\n'
    # print len(learned_results)
    # print len(zero_order_results)
    # print len(learned_results[0])
    # print len(zero_order_results[0])

    for i, q in enumerate(zip(learned_results, zero_order_results)):
        l = q[0]
        z = q[1]
        plotter.figure()
        # TODO: Change xscale to be number of samples, not order
        x = [(j+1)*5 for j in xrange(len(l))]
        plotter.plot(x, l)
        plotter.plot(x, z)
        # TODO: Get the labels here
        test_class = test_labels[i]# 'test class'
        plotter.title('Error for class ' + str(test_class) + ' trained on ' + str(train_class))
        plotter.xlabel('Num samples')
        plotter.ylabel('Planning error')
        plotter.legend(['Learned error', '0order Regressor'],'upper left')
        # TODO: Figure out saving and showing and love and hate and all else
    plotter.show()

def read_and_plot_all_class_files(folder_name):
    files = os.listdir(folder_name)
    # Organize files based on class membership
    class_files = []
    class_set_sizes = []
    class_results = []
    for c in _CLASS_LABELS:
        c_str = c+'_results.txt'
        c_files = []
        for f in files:
            if f.endswith(c_str):
                c_files.append(f)
        c_files_sorted, c_set_sizes = sort_files_by_training_set_size(c_files)
        class_files.append(c_files_sorted)
        class_set_sizes.append(c_set_sizes)
        # Read the results for each of the files
        c_results = []
        for f in c_files_sorted:
            file_name = folder_name+f
            results = read_svm_results_file(file_name)
            c_results.append(results)
        class_results.append(c_results)
        # Plot the results
        plot_class_results(c_results, c_set_sizes, c)

if __name__=='__main__':
    straight_line_dir_name = '/home/thermans/Dropbox/Data/ichr2013-results/straight_line_train_and_validate/variable_class_files/'
    read_and_plot_all_class_files(straight_line_dir_name)
    # spin_dir_name = '/home/thermans/Dropbox/Data/ichr2013-results/rotate_to_heading_train_and_validate/example_files/variable_class_files/'
    # read_and_plot_all_class_files(spin_dir_name)
