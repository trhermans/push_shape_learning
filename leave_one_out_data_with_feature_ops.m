clear
read_files
%for ii = 1:5
%mixture_weight = [0.6 0.65 0.7 0.75 0.8];
mixture_weight = 0.7;
size_hist = 6;
%kparam = [0.5 0.75 1 1.25 1.5 2 2.5];
kparam = 2.5;
jj = 1;

% TODO: Remove zero padding and this stuff
local_start = 14;
local_end = 113;
global_start = 127;

% Run the stuff
for ii = 1:length(kparam)
    for jj=1:length(kparam)
        for i=1:num_sets
            leave_one_out_train{i} = cell2mat([inst(1:i-1) inst(i+1:end)]');
            leave_one_out_test{i} = inst{i};
            leave_one_out_train_lab{i} = log(cell2mat([lab(1:i-1) lab(i+1:end)]'));
            leave_one_out_test_lab{i} = log(lab{i});
            % leave_one_out_train_lab{i} = cell2mat([lab(1:i-1) lab(i+1:end)]') * 100;
            % leave_one_out_test_lab{i} = lab{i} * 100;

            % Binarize global feature
            leave_one_out_train{i}(:,global_start:end) = double(leave_one_out_train{i}(:,global_start:end) > 0);
            leave_one_out_test{i}(:,global_start:end) = double(leave_one_out_test{i}(:,global_start:end) > 0);
            % L1 normalize seems to always help
            leave_one_out_train{i}(:,global_start:end) = (bsxfun(@rdivide, leave_one_out_train{i}(:,global_start:end), sum(leave_one_out_train{i}(:,global_start:end),2)));
            leave_one_out_test{i}(:,global_start:end) = (bsxfun(@rdivide, leave_one_out_test{i}(:,global_start:end), sum(leave_one_out_test{i}(:,global_start:end),2)));
            % Do a Gaussian smoothing on the binarized local histogram
            reshape_local = double(reshape(full(leave_one_out_train{i}(:,local_start:local_end)),size(leave_one_out_train{i},1),10,10) > 0);
            reshape_local_test = double(reshape(full(leave_one_out_test{i}(:,local_start:local_end)),size(leave_one_out_test{i},1),10,10) > 0);
            for j=1:size(leave_one_out_train{i},1)
                sq = squeeze(reshape_local(j,:,:));
                % NOTE: Use to transpose to match C++
                ssi = imresize(sq.', [size_hist,size_hist]);
                ssi = imfilter(ssi,fspecial('gaussian',[5 5],0.2));
                ssi(ssi<0) = 0;
                leave_one_out_train{i}(j,local_start:(local_start+size_hist*size_hist-1)) = ssi(:);
                leave_one_out_train{i}(j,(local_start+size_hist * size_hist):local_end) = 0;
            end
            for j=1:size(leave_one_out_test{i},1)
                rltj = reshape_local_test(j,:,:);
                sq = squeeze(rltj);
                % NOTE: Use to transpose to match C++
                ssi = imresize(sq.', [size_hist,size_hist]);
                ssi = imfilter(ssi,fspecial('gaussian',[5 5],0.2));
                ssi(ssi<0) = 0;
                leave_one_out_test{i}(j,local_start:(local_start+size_hist*size_hist-1)) = ssi(:);
                leave_one_out_test{i}(j,local_start+size_hist * size_hist:local_end) = 0;
            end
            % L1 normalize the local feature
            leave_one_out_train{i}(:,local_start:local_end) = (bsxfun(@rdivide, leave_one_out_train{i}(:,local_start:local_end), sum(leave_one_out_train{i}(:,local_start:local_end),2)));
            leave_one_out_test{i}(:,local_start:local_end) = (bsxfun(@rdivide, leave_one_out_test{i}(:,local_start:local_end), sum(leave_one_out_test{i}(:,local_start:local_end),2)));
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Kernel SVM
            K_train = chi_square_kernel(leave_one_out_train{i}(:,global_start:end),leave_one_out_train{i}(:,global_start:end),2);
            K_test = chi_square_kernel(leave_one_out_test{i}(:,global_start:end),leave_one_out_train{i}(:,global_start:end),2);
            K_train2 = chi_square_kernel(leave_one_out_train{i}(:,local_start:local_end),leave_one_out_train{i}(:,local_start:local_end),2.5);
            K_test2 = chi_square_kernel(leave_one_out_test{i}(:,local_start:local_end),leave_one_out_train{i}(:,local_start:local_end),2.5);
            % C = 0.5 for * 100
            %    svmmodel = libsvm_train(leave_one_out_train_lab{i}, [(1:size(K_train,1))' double(K_train)],'-s 3 -t 4 -c 0.5');
            % C = 5 for log
            K_train = mixture_weight * K_train + (1 - mixture_weight) *  K_train2;
            K_test = mixture_weight * K_test + (1 - mixture_weight) * K_test2;
            svmmodel = libsvm_train(leave_one_out_train_lab{i}, [(1:size(K_train,1))' double(K_train)],['-s 3 -p 0.3 -t 4 -c 2']);
            pred_test{i} = libsvm_predict(leave_one_out_test_lab{i}, [(1:size(K_test,2))' double(K_test)'], svmmodel);
            % pred_test{i} = pred_test{i} + test_out;
            terror{i} = mean(abs(pred_test{i} - leave_one_out_test_lab{i}));
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Sqrting
            %    leave_one_out_train{i}(:,[local_start:local_end global_start:end]) = sqrt(leave_one_out_train{i}(:,[local_start:local_end global_start:end]));
            %    leave_one_out_test{i}(:,[local_start:local_end global_start:end]) = sqrt(leave_one_out_test{i}(:,[local_start:local_end global_start:end]));
            %    leave_one_out_train{i} = leave_one_out_train{i}(:,[local_start:local_start+size_hist*size_hist-1 global_start:end]);
            %    leave_one_out_test{i} = leave_one_out_test{i}(:,[local_start:local_start+size_hist*size_hist-1 global_start:end]);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Boosting
            %    [trees,pred_test{i},terror{i}] = l2boost(full(leave_one_out_train{i}),...
            %    leave_one_out_train_lab{i}, full(leave_one_out_test{i}),...
            %    leave_one_out_test_lab{i},7,1,5e-3);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Linear Regression
            %      LinReg_obj = LinearRegressor_Data((full(leave_one_out_train{i})), leave_one_out_train_lab{i});
            %      w = LinReg_obj.Regress(0.5);
            %      test_out = w(1) + (leave_one_out_test{i}) * w(2:end);
            %      terror{i} = mean(abs(test_out - leave_one_out_test_lab{i}));
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Linear Regression with quadratic mapping
            %      LinReg_obj = LinearRegressor_Data(expand_linear_to_quad((full(leave_one_out_train{i}))), leave_one_out_train_lab{i});
            %      w = LinReg_obj.Regress(20);
            %      test_out = w(1) + expand_linear_to_quad((leave_one_out_test{i})) * w(2:end);
            %      terror{i} = mean(abs(test_out - leave_one_out_test_lab{i}));
        end
        terror = cell2mat(terror);
        %mean_terror(ii,jj) = mean(terror,2);
        mean_terror = mean(terror,2);
        %clear terror
    end
end