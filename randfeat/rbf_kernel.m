function K = rbf_kernel(X, Y, gamma)
rowy = size(Y,1);
rowx = size(X,1);
K = zeros(rowy, rowx);
for i=1:rowx
    for j = i:rowy
        K(j,i) = rbf_func(X(i,:), Y(j,:), gamma);
    end
end
end