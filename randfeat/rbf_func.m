function z = rbf_func(X,Y, gamma)
    diff = X*X'+Y*Y'-2*X*Y';
    z = exp(-gamma*diff);
end